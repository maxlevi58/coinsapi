# CoinGecko API V3 - https://www.coingecko.com/api/documentations/v3#/
API_ENDPOINTS = {
    # ping
    "ping": "/ping",
    # simple
    "simple_price": "/simple/price",
    "simple_token_price": "/simple/token_price/{id}",
    "supported_vs_currencies": "/simple/supported_vs_currencies",
    # coins
    "coin_list": "/coins/list",
    "market_coins": "/coins/markets",
    "single_coin": "/coins/{id}",
    "coin_tickers": "/coins/{id}/tickers",
    "coin_history": "/coins/{id}/history",
    "coin_market_chart": "/coins/{id}/market_chart",
    "coin_market_chart_range": "/coins/{id}/market_chart/range",
    "coin_status_update": "/coins/{id}/status_updates", # BETA
    "coin_ohlc": "/coins/{id}/ohlc",
    # contract
    "coin_contact": "/coins/{id}/contract/{contract_address}",
    "coin_historical_contract": "/coins/{id}/contract/{contract_address}/market_chart/",
    "coin_historical_contract_range": "/coins/{id}/contract/{contract_address}/market_chart/range",
    # categories
    "list_all_categories": "/coins/categories/list",
    "list_market_data_categories": "coins/categories",
    # events
    "events": "/events",
    "events_countries": "/events/countries",
    "event_types": "/events/types",
    # exchange_rates
    "exchange_rates": "/exchange_rates",
    # trending
    "trending_search": "/search/trending",
    # global
    "global": "/global",
    "global_defi": "/global/decentralized_finance_defi",
}

# ALL FALSE: https://api.coingecko.com/api/v3/simple/price?ids=bitcoin&vs_currencies=usd
# ALL TRUE: https://api.coingecko.com/api/v3/simple/price?ids=bitcoin&vs_currencies=usd&include_market_cap=true&include_24hr_vol=true&include_24hr_change=true&include_last_updated_at=true
    

def get_complete_url(api_url, **kwargs):
    # print(f'url: {api_url}, args are: {kwargs}\n')
    api_url += '?'
    for arg in kwargs:
        if (type(kwargs[arg]) == bool):
            kwargs[arg] = str(kwargs[arg]).lower()
        else:
            kwargs[arg] = kwargs[arg].replace(' ', '%20')
        api_url += f'{arg}={kwargs[arg]}&'
    #     print(f'arg: {arg}, content: {kwargs[arg]}, content type: {type(kwargs[arg])}\n')
    # print(f'new url is: {api_url[:-1]}')
    return api_url[:-1]
