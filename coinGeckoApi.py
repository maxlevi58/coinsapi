#!/usr/bin/env python3

from requests import Request, Session
import requests
from requests.exceptions import ConnectionError, Timeout, TooManyRedirects
import json
import sys

from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry

from utils import API_ENDPOINTS, get_complete_url


def main():
  try:
    my = ProAPI()
    # data = my.simple_price('ethereum', 'usd', include_market_cap=True, include_24hr_vol=True, include_24hr_change=True, include_last_updated_at=True)
    data = my.coins_list()
    print(f'data: {data}')
  except (ConnectionError, Timeout, TooManyRedirects) as e:
    print(e)

class ProAPI:
  def __init__(self):
      self.base_url = 'https://api.coingecko.com/api/v3'
      self.headers = {
        'Accepts': 'application/json',
      }
      self.timeout = 120 # seconds
      self.session = requests.session()
      retries = Retry(total=5, backoff_factor=0.5, status_forcelist=[502, 503, 504])
      self.session.mount('http://', HTTPAdapter(max_retries=retries))

  def __request(self, url):
        print(url)
        try:
            response = self.session.get(url, timeout=self.timeout)
        except requests.exceptions.RequestException:
            raise

        try:
            response.raise_for_status()
            content = json.loads(response.content.decode('utf-8'))
            return content
        except Exception as e:
            # check if json (with error message) is returned
            try:
                content = json.loads(response.content.decode('utf-8'))
                raise ValueError(content)
            # if no json
            except json.decoder.JSONDecodeError:
                pass

            raise

### PING ###
  def ping(self):
    """Check API server status"""
    # api_url = '{0}/ping'.format(self.base_url)
    return self.__request(self.base_url + API_ENDPOINTS['ping'])

### SIMPLE ###
  def simple_price(self, ids, vs_currencies, **kwargs): # API_ENDPOINTS['simple_price'] - /simple/price
    """Get the current price of any cryptocurrencies in any other supported currencies that you need."""
    kwargs['ids'] = ids
    kwargs['vs_currencies'] = vs_currencies
    return self.__request(get_complete_url(self.base_url + API_ENDPOINTS['simple_price'], **kwargs))

  def simple_token_price(self, ids, contract_addresses, vs_currencies, **kwargs): # ethereum only # API_ENDPOINTS['simple_token_price'] - /simple/token_price/{id}
    """Get current price of tokens (using contract addresses) for a given platform in any other currency that you need."""
    pass # TODO - Understand whats contract_address

  def supported_vs_currencies(self): # API_ENDPOINTS['supported_vs_currencies'] - /simple/supported_vs_currencies
    """Get list of supported_vs_currencies."""
    return self.__request(self.base_url + API_ENDPOINTS['supported_vs_currencies'])

### COINS ###
  def coins_list(self, **kwargs): # API_ENDPOINTS['coin_list'] - /coins/list
    """List all supported coins id, name and symbol (no pagination required)"""
    """include_platform - boolean flag to include platform contract addresses (eg. 0x… for Ethereum based tokens)."""
    return self.__request(get_complete_url(self.base_url + API_ENDPOINTS['coin_list'], **kwargs))
  
  def market_coins(self, vs_currency, **kwargs): # API_ENDPOINTS['market_coins'] - /coins/markets
    """List all supported coins price, market cap, volume, and market related data"""
    kwargs['vs_currency'] = vs_currency
    return self.__request(get_complete_url(self.base_url + API_ENDPOINTS['market_coins'], **kwargs))

  def single_coin(self, id, **kwargs): # API_ENDPOINTS['single_coin'] - /coins/{id}
    """Get current data (name, price, market, ... including exchange tickers) for a coin"""
    return self.__request(get_complete_url((self.base_url + API_ENDPOINTS['single_coin']).format(id=id), **kwargs))

  def coin_tickers(self, id, **kwargs): # API_ENDPOINTS['coin_tickers'] - /coins/{id}/tickers
    """Get coin tickers (paginated to 100 items)"""
    return self.__request(get_complete_url((self.base_url + API_ENDPOINTS['coin_tickers']).format(id=id), **kwargs))
  
  def coin_history(self, id, date, **kwargs): # API_ENDPOINTS['coin_history'] - /coins/{id}/history
    """Get historical data (name, price, market, stats) at a given date for a coin"""
    kwargs['date'] = date
    return self.__request(get_complete_url((self.base_url + API_ENDPOINTS['coin_history']).format(id=id), **kwargs))

  def coin_market_chart(self, id, vs_currency, days, **kwargs): # API_ENDPOINTS['coin_market_chart'] - /coins/{id}/market_chart
    """Get historical market data include price, market cap, and 24h volume (granularity auto)"""
    kwargs['vs_currency'] = vs_currency
    kwargs['days'] = days
    return self.__request(get_complete_url((self.base_url + API_ENDPOINTS['coin_market_chart']).format(id=id), **kwargs))

  def coin_market_chart_range(self, id, vs_currency, from_date, to_date): # API_ENDPOINTS['coin_market_chart_range'] - /coins/{id}/market_chart/range
    """Get historical market data include price, market cap, and 24h volume within a range of timestamp (granularity auto)"""
    kwargs = {}
    kwargs['vs_currency'] = vs_currency
    kwargs['from_date'] = from_date # Need to convert to UNIX timestamp
    kwargs['to_date'] = to_date # Need to convert to UNIX timestamp
    return self.__request(get_complete_url((self.base_url + API_ENDPOINTS['coin_market_chart_range']).format(id=id), **kwargs))

  def coin_status_update(self, id, **kwargs): # API_ENDPOINTS['coin_status_update'] - /coins/{id}/status_updates
    """Get status updates for a given coin (beta)"""
  
  def coin_ohlc(self, id, vs_currencies, days): # API_ENDPOINTS['coin_ohlc'] - /coins/{id}/ohlc
    """Get coin's OHLC (Beta)"""

### CONTRACT ###
def coin_contract(self, id, contract_address): # API_ENDPOINTS['coin_contract'] - /coins/{id}/contract/{contract_address}
  """Get coin info from contract address"""

def coin_historical_contract(self, id, contract_address, vs_currencies, days): # API_ENDPOINTS['coin_historical_contract'] - /coins/{id}/contract/{contract_address}/market_chart/
  """Get historical market data include price, market cap, and 24h volume (granularity auto) from a contract address"""

def coin_historical_contract_range(self, id, contract_address, vs_currencies, from_date, to_date): # API_ENDPOINTS['coin_historical_contract_range'] - /coins/{id}/contract/{contract_address}/market_chart/range
  """Get historical market data include price, market cap, and 24h volume within a range of timestamp (granularity auto) from a contract address"""

### CATEGORIES ###
def list_all_categories(self): # API_ENDPOINTS['list_all_categories'] - /coins/categories/list
  """List all categories"""

def list_market_data_categories(**kwargs): # API_ENDPOINTS['list_market_data_categories'] - coins/categories
  """List all categories with market data"""

### EVENTS ###
def get_events(self, **kwargs): # API_ENDPOINTS['events'] - /events
  """Get events, paginated by 100"""

def get_event_countries(self): # API_ENDPOINTS['event_countries'] - /events/countries
  """Get list of event countries"""

def get_event_types(self): # API_ENDPOINTS['event_types'] - /events/types
  """Get list of events types"""

### EXCHANGE_RATES ###
def get_exchange_rates(self): # API_ENDPOINTS['exchange_rates'] - /exchange_rates
  """Get BTC-to-Currency exchange rates"""

### TRENDING ###
def trending_search(self): # API_ENDPOINTS['trending_search'] - /search/trending
  """Get trending search coins (Top-7) on CoinGecko in the last 24 hours"""

### GLOBAL ###
def get_global(self): # API_ENDPOINTS['global'] - /global
  """Get cryptocurrency global data"""

def get_global_defi(self): # API_ENDPOINTS['global_defi'] - /global/decentralized_finance_defi
  """Get cryptocurrency global decentralized finance(defi) data"""


if __name__ == '__main__':
  sys.exit(main())